<?php

use Faker\Generator as Faker;

$factory->define(App\Storage::class, function (Faker $faker) {
    $folder_names = [
        'Вища математика',
        'Фізика',
        'English',
        'Психологія',
        'Політологія',
        'ООП',
        'ФП',
        'Дискретна математика',
        'Теорія ймовірностей',
        'Математичний аналіз',
        'Історія України',
        'Українська мова'
    ];
    $subfolder_names = [
        'Лекційні матеріали',
        'Практичні матеріали',
        'Домашні завдання'
    ];
    $file_exts = [
        '.docx',
        '.xlsx',
        '.doc',
        '.docx',
        '.ppt',
        '.pptx',
        '.pdf',
        '.txt'
    ];
    return [
        'type' => $faker->randomElement(['folder', 'image', 'file']),
//        'folder_id' => \App\Storage::where('type', 'folder')->inRandomOrder()->first()->id,
        'author_id' => function($storage)
        {
            if($storage['folder_id'] === null)
                return \App\User::inRandomOrder()->first()->id;
            else
                return \App\Storage::find($storage['folder_id'])->group->users->random()->id;
        },
        'uri' => function($storage) use ($faker, $file_exts)
        {
            switch($storage['type']) {
                case 'folder' : return null;
                case 'image' : return rand(1, 25).'.jpg';
                case 'file' : return $faker->bothify('#???####??????#?##?').$faker->randomElement($file_exts);
            }
        },
        'name' => function($storage) use ($faker, $folder_names, $subfolder_names)
        {
            switch($storage['type']) {
                case 'folder' : return  $faker->randomElement($storage['folder_id'] === null ? $folder_names : $subfolder_names);
                case 'image' : return 'Зображення №'.explode('.', $storage['uri'])[0];
                case 'file' : return 'Документ '.explode('.', $storage['uri'])[1];
            }
        }
    ];
});
