<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    $gender = $faker->boolean;
    return [
        'full_name' => $faker->lastName.' '.$faker->firstName($gender ? 'male' : 'female').' '.$faker->middleName($gender ? 'male' : 'female'),
        'email' => $faker->unique()->safeEmail,
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'group_id' => \App\Group::inRandomOrder()->first()->id,
        'phone' => $faker->phoneNumber,
        'gender' => $gender ? 'men' : 'women',
        'remember_token' => str_random(10),
    ];
});
