<?php

use Illuminate\Database\Seeder;

class Users extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (\App\Group::get(['id']) as $group_id)
            factory(\App\User::class, rand(15, 40))->create(['group_id' => $group_id]);
    }
}
