<?php

use Illuminate\Database\Seeder;

class Groups extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach(['Т', 'С', 'К', 'В', 'О', 'А', 'П'] as $second)
        {
            $group_name = 'І'.$second.'-';
            for($y = 4; $y < 8; $y++)
            {
                for($i = 1; $i < 5; $i++)
                    \App\Group::create(['name' => $group_name.$y.$i]);
            }
        }
    }
}
