<?php

use Illuminate\Database\Seeder;

class Storages extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Storage::class, 500)->create(['folder_id' => null]);
        \App\Storage::where('type', 'folder')->each(function ($storage){
            factory(\App\Storage::class, 10)->create(['folder_id' => $storage->id]);
        });
    }
}
