<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => 'auth'], function() {
    Route::get('/', 'HomeController@index');

    Route::get('/chat', 'ChatController@index');
    Route::post('/chat', 'ChatController@sendMessage');
    Route::post('/chat/getMessages/{id}', 'ChatController@getMessages');

    Route::get('/tasks', 'TaskController@index');

    Route::get('/quiz', 'QuizeController@create')->name('quiz.create');
    Route::post('/quiz', 'QuizeController@store')->name('quiz.store');
//    Route::delete('/quiz/{quiz}', 'ChatController@index');
    Route::post('/quiz/{quiz}/answer', 'QuizeController@answer')->name('quiz.answer');

    Route::post('/update-ava', 'UserController@updateFoto');
    Route::post('/change-password', 'UserController@changePassword');

    Route::prefix('docs')->name('storage.')->group(function () {
        Route::get('/', function () {
            return redirect(route('storage.folder', 'all'));
        })->name('index');
        Route::get('/{mode}/{folder_id?}', 'StorageController@index')->name('folder');
        Route::post('/add/folder', 'StorageController@addFolder')->name('add.folder');
        Route::post('/add/file', 'StorageController@addFile')->name('add.folder');
    });

    Route::group(['middleware' => 'head'], function(){
        Route::get('/new-user', 'UserController@registration_show');
        Route::post('/new-user', 'UserController@registration')->name('user.register');
        Route::post('/new-group-event', 'GroupEventController@store');
        Route::delete('/block-user/{user}', 'UserController@blockUser');
    });
});
//Route::group();
Auth::routes();

Route::get('/admin', function(){
    return view('auth.register');
});

Route::post('/admin', 'GroupController@store')->name('group.register');
