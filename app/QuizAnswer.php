<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class QuizAnswer extends Pivot
{
    protected $table = 'quiz_answers';
}
