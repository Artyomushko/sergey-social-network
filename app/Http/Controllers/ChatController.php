<?php

namespace App\Http\Controllers;

use App\Events\NewMessage;
use App\Message;
use App\User;
use Illuminate\Http\Request;

class ChatController extends Controller
{
    public function index()
    {
        return view('chat', ['users' => \Auth::user()->group_users->where('id', '<>', \Auth::user()->id)->sortBy('full_name')]);
    }

    public function sendMessage(Request $request)
    {
        $message = trim($request->message);
        if(!strlen($message)) return;
        $message = Message::create([
            'from' => \Auth::id(),
            'to' => $request->id === 'group' ? null : $request->id,
            'message' => $request->message
        ]);
        broadcast(new NewMessage($message))->toOthers();
    }

    public function getMessages($id)
    {
        if($id === 'group')
            return \Auth::user()
                ->group
                ->messages()
                ->with('sender')
                ->orderBy('created_at')
                ->get();
        else
            return Message::with('sender')
                ->where(function($query)use($id){
                    $query->where('from', $id)->where('to', \Auth::id());
                })
                ->orWhere(function($query)use($id){
                    $query->where('from', \Auth::id())->where('to', $id);
                })
                ->orderBy('created_at')
                ->get();
    }
}
