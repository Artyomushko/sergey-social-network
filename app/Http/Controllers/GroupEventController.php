<?php

namespace App\Http\Controllers;

use App\GroupEvent;
use Carbon\Carbon;
use Illuminate\Http\Request;

class GroupEventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $uri = null;
        if($request->image !== 'null')
        {
            $upload_dir = 'img/news/';
            $uri = $request->image->hashName();
            $request->image->move($upload_dir, $uri);
        }
        GroupEvent::create([
            'title' => $request->title,
            'text' => $request->text,
            'image' => $uri,
            'date' => $request->date ?? Carbon::now(),
            'group_id' => \Auth::user()->group_id
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\GroupEvent  $groupEvent
     * @return \Illuminate\Http\Response
     */
    public function show(GroupEvent $groupEvent)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\GroupEvent  $groupEvent
     * @return \Illuminate\Http\Response
     */
    public function edit(GroupEvent $groupEvent)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\GroupEvent  $groupEvent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GroupEvent $groupEvent)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\GroupEvent  $groupEvent
     * @return \Illuminate\Http\Response
     */
    public function destroy(GroupEvent $groupEvent)
    {
        //
    }
}
