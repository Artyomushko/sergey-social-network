<?php

namespace App\Http\Controllers;

use App\QuizAnswer;
use App\QuizAnswerVariant;
use App\Quize;
use Illuminate\Http\Request;

class QuizeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('quiz');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $quiz = Quize::create([
            'group_id' => \Auth::user()->group_id,
            'question' => $request->question,
            'type' => $request->type
        ]);

        foreach ($request->answers as $answer)
        {
            QuizAnswerVariant::create([
                'quiz_id' => $quiz->id,
                'answer' => $answer
            ]);
        }

        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Quize  $quize
     * @return \Illuminate\Http\Response
     */
    public function show(Quize $quize)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Quize  $quiz
     * @return \Illuminate\Http\Response
     */
    public function answer(Request $request, Quize $quiz)
    {
        foreach ($request->answers as $answer)
        {
            $ans = QuizAnswerVariant::findOrFail($answer);
            if($ans->quiz_id === $quiz->id)
                QuizAnswer::create(['variant_id' => $ans->id, 'user_id' => \Auth::id()]);
        }
        return redirect('/');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Quize  $quize
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Quize $quize)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Quize  $quize
     * @return \Illuminate\Http\Response
     */
    public function destroy(Quize $quize)
    {
        //
    }
}
