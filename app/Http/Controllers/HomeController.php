<?php

namespace App\Http\Controllers;

use App\Quize;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('index', [
            'groupNews' => \Auth::user()->group->events()->orderBy('date', 'desc')->get(),
            'quizes' => Quize::with('variants')->where('group_id', \Auth::user()->group_id)->orderBy('created_at', 'desc')->get(),
            'answeredQuizes' => \Auth::user()->userAnswers()->get()->map(function($item){return $item->quiz_id;})->unique()
        ]);
    }
}
