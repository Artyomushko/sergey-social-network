<?php

namespace App\Http\Controllers;

use App\QuizAnswerVariant;
use Illuminate\Http\Request;

class QuizAnswerVariantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\QuizAnswerVariant  $quizAnswerVariant
     * @return \Illuminate\Http\Response
     */
    public function show(QuizAnswerVariant $quizAnswerVariant)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\QuizAnswerVariant  $quizAnswerVariant
     * @return \Illuminate\Http\Response
     */
    public function edit(QuizAnswerVariant $quizAnswerVariant)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\QuizAnswerVariant  $quizAnswerVariant
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, QuizAnswerVariant $quizAnswerVariant)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\QuizAnswerVariant  $quizAnswerVariant
     * @return \Illuminate\Http\Response
     */
    public function destroy(QuizAnswerVariant $quizAnswerVariant)
    {
        //
    }
}
