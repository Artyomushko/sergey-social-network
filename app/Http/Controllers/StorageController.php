<?php

namespace App\Http\Controllers;

use App\Storage;
use Illuminate\Http\Request;

class StorageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($mode, $folder_id = null)
    {
        if($folder_id !== null && Storage::findOrFail($folder_id)->author->group_id !== \Auth::user()->group_id)
            return abort(404);
        $storages = null;
        switch ($mode) {
            case 'all':
                $storages = \Auth::user()->groupStorages();
                break;
            case 'my':
                $storages = \Auth::user()->userStorages();
                break;
            case 'photos':
                $storages = \Auth::user()->groupStorages()->photosAndFolders();
                break;
            case 'docs':
                $storages = \Auth::user()->groupStorages()->docsAndFolders();
                break;
            default: return abort(404);
        }
        $storages = $storages->get();
        return view('docs', [
            'storages' => $storages->where('folder_id', $folder_id),
            'storagesCount' => $storages->where('type', '<>', 'folder')->count(),
            'folder_id' => $folder_id,
            'mode' => $mode
        ]);
    }

    public function addFolder(Request $request){
        Storage::create([
            'name' => $request->name,
            'type' => 'folder',
            'folder_id' => $request->folder_id,
            'author_id' => \Auth::id()
        ]);
    }

    public function addFile(Request $request){
        $ext = $request->file->getClientOriginalExtension();
        $type = in_array($ext, Storage::$images) ? 'image' : 'file';
        $upload_dir = $type === 'image' ? 'img/storages/' : 'files/';
        $uri = $request->file->hashName();
        $request->file->move($upload_dir, $uri);

        Storage::create([
            'name' => $request->name ? $request->name : $request->file->getClientOriginalName()     ,
            'type' => $type,
            'folder_id' => $request->folder_id,
            'author_id' => \Auth::id(),
            'uri' => $uri
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Storage  $storage
     * @return \Illuminate\Http\Response
     */
    public function show(Storage $storage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Storage  $storage
     * @return \Illuminate\Http\Response
     */
    public function edit(Storage $storage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Storage  $storage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Storage $storage)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Storage  $storage
     * @return \Illuminate\Http\Response
     */
    public function destroy(Storage $storage)
    {
        //
    }
}
