<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegistrationRequest;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function registration_show()
    {
        return view('new-user');
    }

    public function registration(RegistrationRequest $request)
    {
        User::create([
            'full_name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'group_id' => \Auth::user()->group_id,
            'phone' => $request->phone,
            'gender' => $request->gender
        ]);
        return view('new-user');
    }

    public function updateFoto(Request $request)
    {
        $upload_dir = 'img/users/';
        $uri = $request->foto->hashName();
        $request->foto->move($upload_dir, $uri);

        $user = \Auth::user();

        $user->photo = $uri;
        $user->save();
//        return view('new-user');
    }

    public function blockUser(User $user)
    {
        if($user->id === \Auth::user()->id) return;
        $user->delete();
//        return view('new-user');
    }

    public function changePassword(Request $request)
    {
        $user = \Auth::user();
//        var_dump($user);
        if(!password_verify($request->old, $user->password))
            return abort(419);
        $user->password = bcrypt($request->pswd);
        $user->save();
    }
}
