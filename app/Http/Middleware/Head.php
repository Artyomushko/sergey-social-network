<?php

namespace App\Http\Middleware;

use Closure;

class Head
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(\Auth::user()->isHead())
            return $next($request);
        else return abort(404);
    }
}
