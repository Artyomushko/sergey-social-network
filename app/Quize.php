<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Quize
 *
 * @mixin \Eloquent
 * @property int $id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Quize whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Quize whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Quize whereUpdatedAt($value)
 */
class Quize extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'group_id',
        'question',
        'type'
    ];

    public function variants()
    {
        return $this->hasMany(QuizAnswerVariant::class, 'quiz_id');
    }

//    public function isAnswered()
//    {
//        return $this->hasMany(QuizAnswerVariant::class, 'quiz_id');
//    }

//    public function userAnswers()
//    {
//        return $this->variants;
//    }


}
