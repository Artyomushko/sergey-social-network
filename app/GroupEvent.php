<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\GroupEvent
 *
 * @mixin \Eloquent
 * @property int $id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\GroupEvent whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\GroupEvent whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\GroupEvent whereUpdatedAt($value)
 */
class GroupEvent extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'title',
        'text',
        'image',
        'date',
        'group_id'
    ];

    public function group()
    {
        return $this->belongsTo(Group::class);
    }
}
