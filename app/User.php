<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * App\User
 *
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @mixin \Eloquent
 * @property int $id
 * @property string $full_name
 * @property string $email
 * @property string $password
 * @property int $group_id
 * @property string|null $photo
 * @property string $phone
 * @property string $gender
 * @property string|null $remember_token
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereFullName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 */
class User extends Authenticatable
{

    use SoftDeletes;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'full_name',
        'email',
        'password',
        'group_id',
        'phone',
        'gender'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    private $nameArray;

    private function setNameArray()
    {
        $this->nameArray = explode(' ', $this->full_name);
    }

    public function getFirstName()
    {
        if($this->nameArray === null) $this->setNameArray();
        return $this->nameArray[1];
    }

    public function getLastName()
    {
        if($this->nameArray === null) $this->setNameArray();
        return $this->nameArray[0];
    }

    public function getName()
    {
        return $this->getFirstName().' '.$this->getLastName();
    }

    public function group_users()
    {
        return $this->group->users();
    }

    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    public function userStorages()
    {
        return $this->hasMany(Storage::class, 'author_id');
    }

    public function groupStorages()
    {
        return $this->group->storages();
    }

    public function isHead()
    {
        return $this->group->head_id === $this->id;
    }

    public function userAnswers()
    {
        return $this->belongsToMany(
            'App\QuizAnswerVariant',
            'quiz_answers',
            'user_id',
            'variant_id'
        );
    }
}
