<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Group
 *
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property int|null $head_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Group whereHeadId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Group whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Group whereName($value)
 */
class Group extends Model
{
    protected $fillable = ['name'];
    public $timestamps = false;

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function events()
    {
        return $this->hasMany(GroupEvent::class);
    }

    public function head()
    {
        return $this->belongsTo(User::class);
    }

    public function storages()
    {
        return $this->hasManyThrough(
            'App\Storage',
            'App\User',
            'group_id',
            'author_id');
    }

    public function messages()
    {
        return $this->hasManyThrough(
            'App\Message',
            'App\User',
            'group_id',
            'from')->whereNull('to');
    }
}
