<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\UserEvent
 *
 * @mixin \Eloquent
 * @property int $id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserEvent whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserEvent whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserEvent whereUpdatedAt($value)
 */
class UserEvent extends Model
{
    //
}
