<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Storage
 *
 * @mixin \Eloquent
 * @property int $id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Storage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Storage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Storage whereUpdatedAt($value)
 */
class Storage extends Model
{
    use SoftDeletes;

    public static $images = [
        'jpg',
        'jpeg',
        'png',
        'psd',
        'gif'
    ];

    protected $fillable = [
        'name',
        'type',
        'folder_id',
        'author_id',
        'uri'
    ];

    public function author()
    {
        return $this->belongsTo(User::class);
    }

    public function group()
    {
        return $this->author->group();
    }

    public function scopeNotFolders($query)
    {
        return $query->where('type', '<>', 'folder');
    }

    public function scopeFolders($query)
    {
        return $query->where('type', 'folder');
    }

    public function scopePhotosAndFolders($query)
    {
        return $query->where('type', 'folder')->orWhere('type', 'image');
    }

    public function scopeDocsAndFolders($query)
    {
        return $query->where('type', 'folder')->orWhere('type', 'file');
    }

    public static function getSizeForHuman($bytes)
    {
        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;
    }
}
