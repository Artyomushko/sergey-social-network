<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\QuizAnswerVariant
 *
 * @mixin \Eloquent
 * @property int $id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\QuizAnswerVariant whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\QuizAnswerVariant whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\QuizAnswerVariant whereUpdatedAt($value)
 */
class QuizAnswerVariant extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'quiz_id',
        'answer'
    ];

    public function userAnswers()
    {
        return $this->belongsToMany(
            'App\User',
            'quiz_answers',
            'variant_id',
            'user_id'
        );
    }

    public function quiz()
    {
        return $this->belongsTo(Quize::class);
    }

    public function answers()
    {
        return $this->hasMany(QuizAnswer::class, 'variant_id');
    }

    public function countAnswers()
    {
        return $this->answers->count();
    }
}
