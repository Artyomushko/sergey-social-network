<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Пароль має складатись мінімум з 6 символів та співпадати з полем підтвердження паролю.',
    'reset' => 'Ваш пароль було скинуто!',
    'sent' => 'Ми надіслали Вам листа з посиланням для скидання паролю!',
    'token' => 'Це посилання є недійсним.',
    'user' => "На жаль, ми не можемо знайти користувача з даною електронною скринькою.",

];
