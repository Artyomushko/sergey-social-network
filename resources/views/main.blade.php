<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="/css/app.css">
    <title>{{ config('app.name') }}</title>
</head>
<body>
@yield('header')
@yield('content')
@yield('footer')

<script src="/js/app.js"></script>
@if(Auth::check() && Auth::user()->isHead())
    <script>
        window.blockUser = () => {
            swal({
                title: 'Блокування користувача',
                input: 'select',
                inputOptions: {
                    @foreach(Auth::user()->group->users()->orderBy('full_name')->get() as $user)
                        {{$user->id}}: '{{$user->getName()}}',
                    @endforeach
                },
                text: 'Вкажіть студента: '
            }).then((result) => {
                if(result.value === undefined) return;
                axios.delete('/block-user/' + result.value ).then(location.reload())
            });
        };
    </script>
    <script src="/js/head.js"></script>
@endif
@stack('scripts')
</body>
</html>