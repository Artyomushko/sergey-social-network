@extends('main')

@section('header')
    @include('blocks.header')
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Створення опитування</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('quiz.store') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="question" class="col-md-4 col-form-label text-md-right">Питання</label>

                                <div class="col-md-6">
                                    <input id="question" type="text" class="form-control{{ $errors->has('question') ? ' is-invalid' : '' }}" name="question" value="{{ old('question') }}" required autofocus>

                                    @if ($errors->has('question'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('question') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="type" class="col-md-4 col-form-label text-md-right">Тип опитування</label>

                                <div class="col-md-6 form-inline">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="type" id="type-one" value="one">
                                        <label class="form-check-label" for="type-one">Один варіант відповіді</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="type" id="type-multi" value="multi">
                                        <label class="form-check-label" for="type-multi">Багато варіантів відповідей</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row question-answer">
                                <label class="col-md-4 col-form-label text-md-right">Відповідь: </label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="answers[]" required>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        Створити
                                    </button>
                                    <div class="btn btn-primary" onclick="addAnswerVariant()">
                                        Додати варіант відповіді
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="/js/quiz.js"></script>
@endpush

@section('footer')
    @include('blocks.footer')
@endsection