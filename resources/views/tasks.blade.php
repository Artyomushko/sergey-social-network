@extends('main')

@section('header')
    @include('blocks.header')
@endsection

@section('content')
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light shadow" style="background-color: #e3f2fd;">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <div class="nav-link" id="create-task" onclick="createTask()">Створити задачу</div>
                    </li>
                </ul>
            </div>
        </nav>
        <div class="tasks row shadow bg-light m-3 p-3">
            <h3 class="task-title col-12 font-weight-bold">Завдання 1
                <div class="badge badge-danger">Не виконано</div>
            </h3>
            <div class="col-12 font-italic">
                12-06-2018
            </div>
            <p class="text-justify col-9">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus dapibus
                rhoncus massa, vel varius nisl molestie eget. Curabitur et egestas metus, a lacinia lacus. Etiam posuere
                erat sit amet justo posuere euismod. Nam condimentum non augue vitae porta. Ut ornare diam massa.
                Pellentesque imperdiet, metus eget bibendum finibus, risus arcu mattis quam, et ultricies velit nulla id
                velit. In enim ex, luctus eget mattis sed, lacinia ut sem. Maecenas non vehicula enim. Cras nisl arcu,
                fermentum vel consectetur at, elementum imperdiet eros. Curabitur convallis ac mi quis blandit. Fusce
                nulla mi, tincidunt ac maximus pulvinar, consequat volutpat odio. Donec urna diam, tincidunt vulputate
                arcu ut, pharetra vulputate mi. Fusce id enim tincidunt, bibendum ipsum eget, mollis felis. Nulla eget
                iaculis massa. Ut in ultricies nisi, ut interdum purus. In vulputate tortor pellentesque, blandit ex
                non, vehicula massa.</p>
            <div class="col-3">
                <button class="btn btn-success btn-block">Зробити виконаним</button>
                <button class="btn btn-primary btn-block">Додати підзадачу</button>
            </div>
            <div class="col">
                <h4 class="col-12">Підзадачі:</h4>
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1" checked>
                    <label class="form-check-label" for="exampleCheck1">Підзадача 1</label>
                </div>
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">Підзадача 2</label>
                </div>
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1" checked>
                    <label class="form-check-label" for="exampleCheck1">Підзадача 3</label>
                </div>
            </div>
        </div>
        <div class="tasks row shadow bg-warning m-3 p-3">
            <h3 class="task-title col-12 font-weight-bold">Завдання 3
                <div class="badge badge-danger">Не виконано</div>
            </h3>
            <div class="col-12 font-italic">
                06-06-2018
            </div>
            <p class="text-justify col-9">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus dapibus
                rhoncus massa, vel varius nisl molestie eget. Curabitur et egestas metus, a lacinia lacus. Etiam posuere
                erat sit amet justo posuere euismod. Nam condimentum non augue vitae porta. Ut ornare diam massa.
                Pellentesque imperdiet, metus eget bibendum finibus, risus arcu mattis quam, et ultricies velit nulla id
                velit. In enim ex, luctus eget mattis sed, lacinia ut sem. Maecenas non vehicula enim. Cras nisl arcu,
                fermentum vel consectetur at, elementum imperdiet eros. Curabitur convallis ac mi quis blandit. Fusce
                nulla mi, tincidunt ac maximus pulvinar, consequat volutpat odio. Donec urna diam, tincidunt vulputate
                arcu ut, pharetra vulputate mi. Fusce id enim tincidunt, bibendum ipsum eget, mollis felis. Nulla eget
                iaculis massa. Ut in ultricies nisi, ut interdum purus. In vulputate tortor pellentesque, blandit ex
                non, vehicula massa.</p>
            <div class="col-3">
                <button class="btn btn-success btn-block">Зробити виконаним</button>
                <button class="btn btn-primary btn-block">Додати підзадачу</button>
            </div>
            <div class="col">
                <h4 class="col-12">Підзадачі:</h4>
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1" checked>
                    <label class="form-check-label" for="exampleCheck1">Підзадача 1</label>
                </div>
                {{--<div class="form-check">--}}
                    {{--<input type="checkbox" class="form-check-input" id="exampleCheck1">--}}
                    {{--<label class="form-check-label" for="exampleCheck1">Підзадача 2</label>--}}
                {{--</div>--}}
                {{--<div class="form-check">--}}
                    {{--<input type="checkbox" class="form-check-input" id="exampleCheck1" checked>--}}
                    {{--<label class="form-check-label" for="exampleCheck1">Підзадача 3</label>--}}
                {{--</div>--}}
            </div>
        </div>
        <div class="tasks row shadow bg-light m-3 p-3 bg-dark text-light">
            <h3 class="task-title col-12 font-weight-bold">Завдання 2
                <div class="badge badge-success">Виконано</div>
            </h3>
            <div class="col-12 font-italic">
                02-06-2018
            </div>
            <p class="text-justify col-9">Donec condimentum eu sem ac scelerisque. In viverra, enim id elementum
                ullamcorper, enim metus feugiat massa, in auctor tortor ex in justo. Class aptent taciti sociosqu ad
                litora torquent per conubia nostra, per inceptos himenaeos. Ut sed tortor eu velit iaculis sollicitudin.
                Interdum et malesuada fames ac ante ipsum primis in faucibus. Mauris lorem orci, euismod nec aliquam ac,
                imperdiet sagittis nisi. Vestibulum in hendrerit orci. Proin ut ante a augue facilisis viverra.</p>
            <div class="col-3">
                <button class="btn btn-danger btn-block">Зробити невиконаним</button>
                <button class="btn btn-primary btn-block disabled">Додати підзадачу</button>
            </div>
            <div class="col">
                <h4 class="col-12">Підзадачі:</h4>
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1" disabled checked>
                    <label class="form-check-label" for="exampleCheck1">Підзадача 1</label>
                </div>
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1" disabled checked>
                    <label class="form-check-label" for="exampleCheck1">Підзадача 2</label>
                </div>
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1" disabled checked>
                    <label class="form-check-label" for="exampleCheck1">Підзадача 3</label>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    @include('blocks.footer')
@endsection

@push('scripts')
    <script src="/js/docs.js"></script>
    <script>
        window.createTask = () => {
            swal.mixin({
                title: 'Створення завдання',
                progressSteps: [1, 2, 3, 4],
                confirmButtonText: 'Далі'
            }).queue([
                {
                    input: 'text',
                    text: 'Вкажіть назву завдання: '
                },
                {
                    input: 'textarea',
                    text: 'Опишіть завдання: '
                },
                {
                    html: '<input type="color"><br><span>Оберіть колір</span>',
                    text: 'Оберіть колір: '
                },
                {
                    html: '<input type="date" name="date" id="news-date">',
                    text: 'Оберіть дату виконання: '
                }
            ]);
        };
        window.createSubtask = () => {
            swal({
                title: 'Створення підзадачі',
                input: 'text',
                text: 'Вкажіть назву підзадачі: '
            });
        };
    </script>
@endpush