<div class="container footer">
<nav class="navbar navbar-expand-lg navbar-dark bg-dark footer" style="background-color: #e3f2fd;">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav nav-fill flex-fill">
            <li class="nav-item">
                <a class="nav-link" href="/">Головна</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/chat">Чат</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/docs">Матеріали</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/calend">Календар</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/tasks">Задачі</a>
            </li>
        </ul>
    </div>
</nav>
</div>