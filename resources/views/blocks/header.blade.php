<div class="modal fade" id="user-modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Блок користувача@if(Auth::user()->isHead()) (староста)@endif</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <a href="{{ route('quiz.create') }}" class="btn btn-outline-secondary btn-block">
                    Додати опитування
                </a>
                @if(Auth::user()->isHead())
                    {{--<div class="btn btn-outline-secondary btn-block">--}}
                        {{--Видалити опитування--}}
                    {{--</div>--}}
                    <div class="btn btn-outline-secondary btn-block" onclick="createNews()">
                        Додати новину
                    </div>
                @endif
                <div class="btn btn-outline-secondary btn-block" onclick="uploadFoto()">
                    Змінити фото
                </div>
                @if(Auth::user()->isHead())
                    <a href="/new-user" class="btn btn-outline-secondary btn-block">
                        Реєстрація студента
                    </a>
                    <div class="btn btn-outline-secondary btn-block" onclick="blockUser()">
                        Блокування користувача
                    </div>
                @endif
                <div class="btn btn-outline-secondary btn-block" onclick="changePassword()">
                    Змінити пароль
                </div>
                <div class="btn btn-outline-secondary btn-block" onclick="$('#logout-form').submit()">
                    Вийти
                </div>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрити</button>
            </div>
        </div>
    </div>
</div>

<div class="container header">
    <div class="row">
        <div class="col-auto">
            <a href="/"><img src="/img/kpi.png" alt="" class="logo"></a>
        </div>
        <div class="col">
            <h1>{{ config('app.name') }}</h1>
            <nav class="navbar navbar-expand-lg navbar-light shadow mb-1 rounded" style="background-color: #e3f2fd;">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                        aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link" href="/">Головна</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/chat">Чат</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/docs">Матеріали</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/calend">Календар</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/tasks">Задачі</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        <div class="col-auto">
            <div class="row">
                <a href="#" data-toggle="modal" data-target="#user-modal"><img src="/img/users/{{ Auth::user()->photo }}" alt=""
                                                                               class="rounded-circle"
                                                                               style="max-width: 100px;"></a>
            </div>
            <div class="row">{{ Auth::user()->getName() }}</div>
        </div>
    </div>
</div>