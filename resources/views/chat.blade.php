@extends('main')

@section('header')
    @include('blocks.header')
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-4 interlocators">
                <div class="interlocator p-0 m-3 rounded shadow row align-items-center" data-user="group"><img
                            src="/img/kpi.png" alt=""
                            class="rounded-circle m-2">{{ Auth::user()->group->name }}
                </div>
                @foreach($users as $user)
                    <div class="interlocator p-0 m-3 rounded shadow row align-items-center" data-user="{{ $user->id }}">
                        <img
                                src="/img/users/{{ $user->photo }}" alt=""
                                class="rounded-circle m-2">{{ $user->getName() }}
                    </div>
                @endforeach
            </div>
            <div class="col chat-chat-overlay">
                <div class="col chat-chat-scroll">
                    <div class="col bg-dark text-light chat-chat"></div>
                </div>
                <textarea name="" id="" cols="30" rows="3" class="form-control"
                          placeholder="Ваше повідомлення..."></textarea>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    @include('blocks.footer')
@endsection

@push('scripts')
    <script>
        const userId = {{ Auth::id() }};
        const groupId = {{ Auth::user()->group_id }};
    </script>
    <script src="/js/chat.js"></script>
@endpush