@extends('main')

@section('header')
    @include('blocks.header')
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-3 hide-scrolling">
                <div class="quizes">
                    <h3>Опитування</h3>
                    @foreach($quizes as $quiz)
                        @if(!in_array($quiz->id, $answeredQuizes->toArray()))
                            <form action="{{ route('quiz.answer', $quiz->id) }}" method="post"
                                  class="test shadow p-3 mb-3 rounded"><p>{{ $quiz->question }}</p>
                                @csrf
                                @foreach($quiz->variants as $variant)
                                    <input type="{{ $quiz->type === 'one' ? 'radio' : 'checkbox' }}"
                                           value="{{ $variant->id }}" name="answers[]"> {{ $variant->answer }}<br>
                                @endforeach
                                <input type="submit" class="btn btn-light">
                            </form>
                        @else
                            <div class="test shadow p-3 mb-3 rounded"><p>{{ $quiz->question }}</p>
                                @foreach($quiz->variants as $variant)
                                    <div> ({{ $variant->countAnswers() }}) {{ $variant->answer }}</div>
                                @endforeach
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
            <div class="col-6 hide-scrolling">
                <div class="group-news">
                    @foreach($groupNews as $news)
                        <div class="main-news shadow p-3 mb-3 bg-white rounded">
                            @if($news->image !== null)<img src="/img/news/{{ $news->image }}" alt=""
                                                           class="img-thumbnail float-left">@endif
                            <h3>{{ $news->title }}</h3>
                            <p>{{ $news->text }}</p>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="col-3">
                <h3 class="text-center">Нагадування</h3>
                <div class="notification shadow p-3 mb-3 rounded bg-warning">
                    <b>Завдання 3</b>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus dapibus
                        rhoncus massa, vel varius nisl molestie eget. Curabitur et egestas metus, a lacinia lacus. Etiam posuere
                        erat sit amet justo posuere euismod.</p>
                    <i class="notification-time">06 червня 2018</i>
                </div>
                <div class="notification shadow p-3 mb-3 rounded">
                    <b>Завдання 1</b>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus dapibus
                        rhoncus massa, vel varius nisl molestie eget. Curabitur et egestas metus, a lacinia lacus. Etiam posuere
                        erat sit amet justo posuere euismod.</p>
                    <i class="notification-time">12 червня 2018</i>
                </div>
                {{--<div class="notification shadow p-3 mb-3 rounded important">--}}
                    {{--<b>Екзамен</b>--}}
                    {{--<p></p>--}}
                    {{--<i class="notification-time">24 травня 2018, 09:00</i>--}}
                {{--</div>--}}
            </div>
        </div>
    </div>
@endsection

@section('footer')
    @include('blocks.footer')
@endsection