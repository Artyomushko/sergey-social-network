@extends('main')

@section('header')
    @include('blocks.header')
@endsection

@section('content')
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light shadow" style="background-color: #e3f2fd;">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link @if($mode === 'all') active @endif" href="{{ route('storage.folder', ['mode' => 'all', 'folder_id' => $folder_id]) }}">Всі документи@if($mode === 'all'): {{ $storagesCount }} @endif</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link @if($mode === 'my') active @endif" href="{{ route('storage.folder', ['mode' => 'my', 'folder_id' => $folder_id]) }}">Мої документи@if($mode === 'my'): {{ $storagesCount }} @endif</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link @if($mode === 'photos') active @endif" href="{{ route('storage.folder', ['mode' => 'photos', 'folder_id' => $folder_id]) }}">Фото@if($mode === 'photos'): {{ $storagesCount }} @endif</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link @if($mode === 'docs') active @endif" href="{{ route('storage.folder', ['mode' => 'docs', 'folder_id' => $folder_id]) }}">Файли@if($mode === 'docs'): {{ $storagesCount }} @endif</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="">Сортувати по даті <i class="fa fa-angle-down"></i></a>
                    </li>
                </ul>
                <form class="form-inline">
                    <input type="text" class="form-control">
                    <input type="submit" value="Пошук" class="btn btn-outline-secondary">
                </form>
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <div class="nav-link" id="create-folder">Створити папку</div>
                    </li>
                    <li class="nav-item">
                        <div class="nav-link" id="upload-file">Завантажити файл</div>
                    </li>
                </ul>
            </div>
        </nav>
        <div class="m-3 row">
            @foreach($storages->where('type', 'folder') as $storage)
            <a href="{{ route('storage.folder', [$mode, $storage->id]) }}" class="docs-item docs-folder col-2 shadow p-3 m-3 rounded">{{ $storage->name }}</a>
            @endforeach
        </div>
        <div class="m-3 row">
            @foreach($storages->where('type', '<>', 'folder') as $storage)
                @php
                    $updated_at = \Carbon\Carbon::parse($storage->updated_at);
                @endphp
                @if($storage->type === 'image')
                    <a href="{{ asset('/img/storages/'.$storage->uri) }}" class="docs-item docs-file col-2 shadow p-3 m-3 rounded docs-image">
                        <div class="docs-image-inner">
                            <span class="vertical-aligner"></span>
                            <img src="/img/storages/{{ $storage->uri }}" alt="">
                        </div>
                        <div class="docs-info">
                            <b>{{ $storage->name }}</b><br>
                            <i>{{ \App\Storage::getSizeForHuman(File::size('img/storages/'.$storage->uri)) }}</i><br>
                            <i>{{ $updated_at->format('d ').__('date.monthsWhom.'.($updated_at->format('n') - 1)).$updated_at->format(' Y H:i') }}</i>
                        </div>
                    </a>
                @elseif($storage->type === 'file')
                    @php
                        $file_ext = explode('.', $storage->uri)[1];
                    @endphp
                    <a href="/files/{{ $storage->uri }}" class="d-block docs-item docs-file col-2 shadow p-3 m-3 rounded docs-{{ $file_ext }}">
                        <div class="docs-info">
                            <b>{{ $storage->name }}</b><br>
                            <i>255 Kb</i><br>
                            <i>{{ $updated_at->format('d ').__('date.monthsWhom.'.($updated_at->format('n') - 1)).$updated_at->format(' Y H:i') }}</i>
                        </div>
                    </a>
                @endif
            @endforeach
        </div>
    </div>
@endsection

@section('footer')
    @include('blocks.footer')
@endsection

@push('scripts')
    <script src="/js/docs.js"></script>
    <script>
        window.folder_id = '{{ $folder_id }}';
    </script>
@endpush