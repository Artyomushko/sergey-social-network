
window._ = require('lodash');
window.Popper = require('popper.js').default;

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
    window.$ = window.jQuery = require('jquery');

    require('bootstrap');
    window.swal = require('sweetalert2');
} catch (e) {}

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

import Echo from 'laravel-echo'

window.io = require('socket.io-client');

window.Echo = new Echo({
    broadcaster: 'socket.io',
    host: window.location.hostname + ':6001',
    auth: {
        headers: {
            Authorization: 'Bearer ' + $('meta[name="csrf-token"]').attr('content')
        },
    },
});

window.uploadFoto = () => {
    swal({
        title: 'Завантаження фото',
        input: 'file',
        text: 'Оберіть файл: '
    }).then((result) => {
        if (!result.value) {
            swal('Ви не обрали фото');
            return;
        }
        let data = new FormData;
        data.append('foto', result.value);
        axios.post('/update-ava', data).then(/*location.reload()*/)
    });
};

window.changePassword = () => {
    swal.mixin({
        title: 'Змінення паролю',
        progressSteps: [1, 2, 3],
        confirmButtonText: 'Далі'
    }).queue([
        {
            input: 'password',
            text: 'Вкажіть поточний пароль: '
        },
        {
            input: 'password',
            text: 'Вкажіть новий пароль: '
        },
        {
            input: 'password',
            text: 'Вкажіть новий пароль ще раз: '
        }
    ]).then((result) => {
        if (!result.value[0] || !result.value[1] || !(result.value[1] === result.value[2])) {
            swal('Ви ввели неправильний пароль');
            return;
        }
        axios.post('/change-password', {
            old: result.value[0],
            pswd: result.value[1]
        }).then((data) => {
            swal('Пароль успішно змінено!');
        }).catch(() => {
            swal('Виникла помилка!');
        });
    });
};