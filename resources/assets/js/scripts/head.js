window.createNews = () => {
    swal.mixin({
        title: 'Створення новини',
        progressSteps: [1, 2, 3, 4],
        confirmButtonText: 'Далі'
    }).queue([
        {
            input: 'text',
            text: 'Вкажіть назву новини: '
        },
        {
            input: 'textarea',
            text: 'Напишіть текст для новини: '
        },
        {
            input: 'file',
            text: 'Оберіть фотографію (не обов\'язково): '
        },
        {
            html: '<input type="date" name="date" id="news-date">',
            text: 'Оберіть дату події (не обов\'язково): '
        }
    ]).then((result) => {
        if(!result.value[0] || !result.value[1]){
            swal('Треба описати новину та назвати її');
            return;
        }
        let data = new FormData;
        data.append('title', result.value[0]);
        data.append('text', result.value[1]);
        data.append('image', result.value[2]);
        data.append('date', $('#news-date').val());
        axios.post('/new-group-event', data).then(/*location.reload()*/)
    });
};