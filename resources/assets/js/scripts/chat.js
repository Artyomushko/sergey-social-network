Echo.private('chat.' + userId)
    .listen('NewMessage', (message) => {
        newMessage(message, message.message.from);
    });

Echo.private('chat.group.' + groupId)
    .listen('NewMessage', (message) => {
        newMessage(message);
    });

$('.interlocator').click(function(){
    $('.interlocator').removeClass('bg-dark').removeClass('text-light');
    $(this).addClass('bg-dark').addClass('text-light').removeClass('bg-success');
    $('.chat-chat').html('');

    axios.post('/chat/getMessages/' + $(this).data('user'))
        .then((messages) => {
            messages.data.forEach((message) => {
                const nameArray = message.sender.full_name.split(' ');
                message.sender.name = [nameArray[1], nameArray[0]].join(' ');
                if(message.from === userId)
                    $('.chat-chat').append('<div class="chat-message chat-message-outer shadow rounded bg-secondary">' + message.message + '</div>' +
                        '                <div class="clearfix"></div>');
                else
                    $('.chat-chat').append('<div class="chat-message chat-message-inner shadow rounded bg-light text-dark row">' +
                        '                    <img src="/img/users/' + message.sender.photo + '" alt="" class="rounded-circle m-2 chat-message-foto">' +
                        '                    <div class="col">' +
                        '                        <p><b><i>' + message.sender.name + '</i></b></p>' +
                        '                        <p>' + message.message + '</p>' +
                        '                    </div>' +
                        '                </div>' +
                        '                <div class="clearfix"></div>');
            });
            $('.chat-chat-scroll').scrollTop($('.chat-chat').height());
        });
});

function newMessage(message, interlocatorId)
{
    interlocatorId = interlocatorId || 'group';
    const interlocator = $('.interlocator[data-user=' + interlocatorId + ']');

    if($(interlocator).hasClass('bg-dark')) $(interlocator).click();
    else $(interlocator).addClass('bg-success');
}

$('textarea').keypress(function (e) {
    if(e.keyCode === 13)
    {
        e.preventDefault();
        const message = $(this).val();
        $(this).val('');

        axios.post('', {
            message: message,
            id: $('.interlocator.bg-dark').data('user')
        }).then(() => $('.interlocator.bg-dark').click());
    }
});