$('#create-folder').click(() => {
    swal({
        title: 'Створення папки',
        text: 'Вкажіть назву папки: ',
        input: 'text'
    }).then((result) => {
        if(!result.value) return;
        axios.post('/docs/add/folder', {
            name: result.value,
            folder_id: window.folder_id
        }).then(location.reload())
    });
});

$('#upload-file').click(() => {
    swal.mixin({
        title: 'Завантаження файлу',
        progressSteps: [1, 2],
        confirmButtonText: 'Далі'
    }).queue([
        {
            input: 'file',
            text: 'Оберіть файл: '
        },
        {
            input: 'text',
            text: 'Вкажіть назву файлу: '
        }
    ]).then((result) => {
        if(!result.value[0]){
            swal('Ви не обрали файл');
            return;
        }
        let data = new FormData;
        data.append('name', result.value[1]);
        data.append('folder_id', window.folder_id);
        data.append('file', result.value[0]);
        axios.post('/docs/add/file', data).then(location.reload())
    });
});