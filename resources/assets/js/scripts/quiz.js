window.addAnswerVariant = () => {
    const answer = $('.question-answer').last().clone();
    answer.find('input').val('');
    $('.question-answer').last().after(answer);
};